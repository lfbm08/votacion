<?php

declare(strict_types=1);

namespace usuario\Form;

use Laminas\Form\Element;
use Laminas\Form\Form;

class RegisterForm extends Form
{
    public function init()
    {
        parent::init();
        $this->setName('nuevo_voto');
        $this->setAttribute('method', 'post');

        $this->add([
            'type' => Element\Text::class,
            'name' => 'cedula',
            'options' => [
                'label' => 'Cedula'
            ],
            'attributes' => [
                'required' => true,
                'placeholder' => 'Ingrese su cedula',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'type' => Element\Select::class,
            'name' => 'postulantes',
            'options' => [
                'label' => 'Postulantes',
                'empty_option' => 'Seleccione',
                'value_options' => [
                    'Luisa' => 'Luisa',
                    'Carlos' => 'Carlos'
                ]
            ],
            'attributes' => [
                'required' => true,
                'class' => 'custom-select'
            ]
        ]);

        $this->add([
            'type' => Element\Submit::class,
            'name' => 'registrarVoto',
            'attributes' => [
                'value' => 'Registrar Voto',
                'class' => 'btn btn-primary btn-lg btn-block'
            ]
        ]);
    }
}
