<?php

declare(strict_types=1);

namespace usuario\Handler;

use Mezzio\Template\TemplateRendererInterface;
use Psr\Container\ContainerInterface;
use usuario\Form\RegisterForm;
use usuario\Model\Table\UsersTable;

class RegisterHandlerFactory
{
    public function __invoke(ContainerInterface $container) : RegisterHandler
    {
        return new RegisterHandler(
            $container->get('FormElementManager')->get(RegisterForm::class),
            $container->get(TemplateRendererInterface::class),
            $container->get(UsersTable::class)
        );
    }
}
