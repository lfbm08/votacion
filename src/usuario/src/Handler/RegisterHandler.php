<?php

declare(strict_types=1);

namespace usuario\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\RedirectResponse;
use Mezzio\Template\TemplateRendererInterface;
use usuario\Form\RegisterForm;
use usuario\Model\Table\UsersTable;

class RegisterHandler implements MiddlewareInterface
{
    /**
     * @var TemplateRendererInterface
     */
    private $renderer;

    private $registerForm;

    private $usersTable;

    public function __construct(RegisterForm $registerForm, TemplateRendererInterface $renderer, UsersTable $usersTable)
    {
        $this->registerForm = $registerForm;
        $this->renderer = $renderer;
        $this->usersTable = $usersTable;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface {
        if ($request->getMethod() == 'POST') {
            $this->registerForm->setData($request->getParsedBody());
            if ($this->registerForm->isValid()) {
                $this->usersTable->insertarVoto($request->getParsedBody());
                $response = $handler->handle($request);
                if ($response->getStatusCode() !== 302) {
                    return new RedirectResponse('/');
                }
                return $response;
            }
        }
        return new HtmlResponse($this->renderer->render(
            'usuario::register',
            [
                'form' => $this->registerForm
            ]
        ));
    }
}
