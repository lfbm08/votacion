<?php

declare(strict_types=1);

namespace usuario\Model\Table;

use Laminas\Db\TableGateway\AbstractTableGateway;
use Laminas\Db\Adapter\Adapter;

class UsersTable extends AbstractTableGateway {
    protected $table = 'voto';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->initialize();
    }

    public function insertarVoto(array $data) {
        $values = [
            'id_entidad_postulante' => $data['postulantes'],
            'id_entidad_votante' => $data['cedula'],
            'fecha' => date('Y-m-d')
        ];

        $sqlInsert = $this->sql->insert()->values($values);
        $sqlState = $this->sql->prepareStatementForSqlObject($sqlInsert);

        return $sqlState->execute();
    }

    
}