# Sistema de Registro de Votos
¡Bienvenido!
A continuación te indicaré las instrucciones para la instalación del proyecto.

## Requerimientos

1. Composer
2. XAMPP (PHP 7.4 y MySQL)

## Proceso de Instalación local

1. Si te encuentras en Windows dirigete a la carpeta `C:\xampp\htdocs`.

2. Clona el repositorio.

```bash
$ git clone [SSH o HTTPS de proyecto]
$ cd votacion
```
3. Para correr el proyecto debes ejecutar el siguiente comando: 

```bash
$ composer run --timeout=0 serve
```
4. Luego, ingresa en tu navegador a http://localhost:8080/ingresar y ya está!

## Posibles errores

* Si no se descargan todos los archivos automáticamente puedes correr el siguiente comando:

```bash
$ composer install
```
* Si presentas errores con la base de datos local debes crear un archivo llamado `development.local.php` en la carpeta `\config\autoload\` y agregar lo siguiente:

```
<?php

declare(strict_types=1);

use Mezzio\Container;
use Mezzio\Middleware\ErrorResponseGenerator;

return [
    'dependencies' => [
        'factories' => [
            ErrorResponseGenerator::class => Container\WhoopsErrorResponseGeneratorFactory::class,
            'Mezzio\Whoops'               => Container\WhoopsFactory::class,
            'Mezzio\WhoopsPageHandler'    => Container\WhoopsPageHandlerFactory::class,
        ],
    ],
    'whoops'       => [
        'json_exceptions' => [
            'display'    => true,
            'show_trace' => true,
            'ajax_only'  => true,
        ],
    ],
    'db' => [
        'username' => 'root',
        'password' => '',
    ]
];
```